import numpy as np
from numpy import array as arr
from brs import parse_brs


def make_vec(p1, p2):
    return p2 - p1


def scal_mult(v1, v2):
    return v1.dot(v2)


def vec_mult(v1, v2):
    i = v1[1] * v2[2] - v1[2] * v2[1]
    j = v1[0] * v2[2] - v1[2] * v2[0]
    k = v1[0] * v2[1] - v1[1] * v2[0]
    return arr([i, j, k])


def dots2coeff(points):
    p1, p2, p3 = points
    e1 = make_vec(p1, p2)
    e2 = make_vec(p1, p3)
    norm = vec_mult(e1, e2)
    v1 = make_vec(arr([0, 0, 0]), p1)
#    v1 = make_vec(p1, arr([0, 0, 0]))
    b = scal_mult(v1, norm)
    return (norm.tolist(), b)


def convert(f):
    """\
    takes file with BRS, parses it to triangles
    and prepares simplex coefficients in canonical form.
    Yields a list of tuples:
    (coefficients, index of A matrix with 1 in it), free coefficient).
    """
    for i, x in enumerate(parse_brs(f), 1):
        aline, b = dots2coeff(x)
        if b < 0:
            b = - b
            aline = [-j for j in aline]
        yield ((aline, i + len(aline)), b)
