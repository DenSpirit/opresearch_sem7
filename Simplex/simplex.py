from convert import convert
from math import inf
from pdb import set_trace as st
from sys import argv


def has(pred, col):
    for x in col:
        if pred(x):
            return True
    return False


def simplex(N, B, A, b, c, x):
    iterations = 0
    def c_pos(i):
        return c[i] > 0
    while (has(c_pos, N)):
        iterations += 1
        e = next(filter(c_pos, N))
        delta = inf
        for i in B:
            if A[i, e] > 0:
                dn = b[i] / A[i, e]
                if dn < delta:
                    l = i
                    delta = dn
        if delta != inf:
            N, B, A, b, c = pivot(N, B, A, b, c, l, e)
        else:
            raise Exception("unbounded")

    for i in range(min(x), len(x)):
        if i in B:
            x[i] = b[i]
        else:
            x[i] = 0

    return (iterations, x)


def pivot(N, B, A, b, c, l, e):
    print("{} out, {} in".format(l, e))
    bA = dict()
    aA = dict()
    cA = dict()
    # "вычисление коэффициентов уравнения для новой базисной переменной x[e]"
    bA[e] = b[l] / A[l, e]
    for j in N - {e}:
        aA[e, j] = A[l, j] / A[l, e]
    aA[e, l] = 1 / A[l, e]
    # вычисление коэффициентов остальных ограничений
    for i in B - {l}:
        bA[i] = b[i] - A[i, e] * bA[e]
        for j in N - {e}:
            aA[i, j] = A[i, j] - A[i, e] * aA[e, j]
        aA[i, l] = - A[i, e] * aA[e, l]
    # вычисление целевой функции
    for j in N - {e}:
        cA[j] = c[j] - c[e] * aA[e, j]
    cA[l] = -c[e] * aA[e, l]
    # вычисление новых множеств базисных и небазисных переменных
    NA = (N - {e}) | {l}
    BA = (B - {l}) | {e}
    return NA, BA, aA, bA, cA


def divider(A):
    def dividebyone(x):
        if x == 0:
            return 0
        else:
            return 1 / x
    for key, val in A.items():
        A[key] = dividebyone(val)


def main():
    with open(argv[1], 'r') as f:
        A = {}
        b = {}
        for i, (alinepoint, bcoeff) in enumerate(convert(f), 4):
            aline, onepoint = alinepoint
            A[i, onepoint] = 1
            for j, a in enumerate(aline, 1):
                A[i, j] = a
            b[i] = bcoeff

        # A elements are - surprise - 1/a[i][j]
        #divider(A)

        c = {1: 50, 2: 40, 3: 20}
        for key in b:
            c[key] = 0
        x = {1: 0, 2: 0, 3: 0}
        x.update(b)

        N = {1, 2, 3}
        B = set(range(4, len(x)))
        iternum, result = simplex(N, B, A, b, c, x)
        print("{} iterations total".format(iternum))
        x = [result[i] for i in [1, 2, 3]]
        print("x={},y={},z={}".format(*tuple(x)))
        f = 0
#        for i, ci in enumerate([50, 40, 20]):
#            f += ci * x[i]
        print("f(x,y,z)={}".format(f))


if __name__ == '__main__':
    main()
