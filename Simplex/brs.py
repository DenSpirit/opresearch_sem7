import numpy as np
from itertools import islice

POINTS_SECTION = 'vertices'
TRIANGLES_SECTION = 'triangles'


def extract_vectors(triangle):
    refs = list(triangle)
    refs.sort()
    res = []
    res.append((refs[0],refs[1]))
    res.append((refs[0],refs[2]))
    res.append((refs[1],refs[2]))
    return res


def read_points(it):
    while True:
        p = list(islice(it, 0, 3))
        if len(p) == 3:
            yield p
        else:
            raise StopIteration


def make_three(it):
    while True:
        t = tuple(islice(it, 0, 3))
        if len(t) == 3:
            yield t
        else:
            raise StopIteration


def deref_triangles(points, triangles):
    for ref in triangles:
        r1, r2, r3 = ref
        yield points[r1], points[r2], points[r3]


def spam_numbers(f, conv):
    stripped = (line.strip() for line in f)
    for line in stripped:
        if line == '':
            raise StopIteration
        else:
            for x in line.split():
                yield conv(x)


def parse_brs(f):
    """\
    produces a list of triangles from BRS file passed as parameter.
    BRS file is read as text,
    first line starting with 'vertices' indicates start of input,
    empty line indicates end of vertices.
    Any excess points (not divisible by 3) are ignored.
    Triangle is a tuple of 3 points.
    Point is a numpy array of 3 floats
    """
    brs = BRS._read_file(f)
    points = [np.array(point) for point in brs.points]
    triangles = deref_triangles(points, brs.triangles)

    return triangles


def skip_until(it, string):
    for line in it:
        if line.startswith(string):
            break
    else:
        raise Exception(string + ' not found')
    return it


class BRS:

    def __init__(self, points, triangles):
        self.points = points
        self.triangles = triangles

    def _read_file(f):
        skip_until(f, POINTS_SECTION)
        coordinates = spam_numbers(f, float)
        points = list(read_points(coordinates))
        skip_until(f, TRIANGLES_SECTION)
        triangles = list(make_three(spam_numbers(f, int)))
        return BRS(points, triangles)
    _read_file = staticmethod(_read_file)

    def read(path):
        with open(path, 'r') as f:
            return BRS._read_file(f)
    read = staticmethod(read)


    def compress(self):
        pointmap = {}
        nextFreePoint = 0
        for t in self.triangles:
            for p in list(t):
                if p not in pointmap:
                    pointmap[p] = nextFreePoint
                    nextFreePoint += 1

        newpoints = [0] * nextFreePoint
        
        for old,new in pointmap.items():
            newpoints[new] = self.points[old]


        def mapt(t):
            p1,p2,p3 = t
            return pointmap[p1],pointmap[p2],pointmap[p3]


        newtriangles = [mapt(t) for t in self.triangles]
        self.points = newpoints
        self.triangles = newtriangles


    def write(self,path):
        with open(path,'w') as f:
           f.write('{} {}\n'.format(POINTS_SECTION, len(self.points)))
           for vert in self.points:
               f.write('{} {} {}\n'.format(*vert))

           f.write('\n')

           f.write('{} {}\n'.format(TRIANGLES_SECTION, len(self.triangles)))
           for t in self.triangles:
               f.write('{} {} {}\n'.format(*t))

    def to_graph(self):
        edgeAdj = {}

        for triangle in self.triangles:
            for refvec in extract_vectors(triangle):
                adj = edgeAdj.setdefault(refvec,{triangle})
                adj.add(triangle)

        vertAdj = dict()
        for triaglist in edgeAdj.values():
            for triag in triaglist:
                adjlist = vertAdj.setdefault(triag,set())
                adjlist.update(triaglist)
                adjlist.remove(triag)

        return vertAdj


