import sys
sys.path.insert(0,'../Simplex')
from brs import BRS
from collections import deque


def main():
    outfmt = sys.argv[2] + '-{}.brs'
    mccCount = 0
    print('reading {}'.format(sys.argv[1]))
    brs = BRS.read(sys.argv[1])
    fmt = sys.argv[1],len(brs.points),len(brs.triangles)
    print('read {}, got {} points, {} triangles'.format(*fmt))

    vertAdj = brs.to_graph()

    best = set()
    while len(vertAdj) > 0:
        vec_q = deque()
        vec_q.append(next(iter(vertAdj.keys())))
        cur = set()
        while len(vec_q) > 0:
            vert = vec_q.popleft()
            cur.add(vert)
            for t in vertAdj[vert]:
                if not t in cur:
                    vec_q.append(t)
                    cur.add(t)
        
        print('{}-sized component'.format(len(cur)))

        if len(cur) > len(best):
            print('{}>{}'.format(len(cur),len(best)))
            best = cur
        else:
            print('{}<{}'.format(len(cur),len(best)))

        triangles = list(cur)
        nbrs = BRS(brs.points,list(cur))
        nbrs.compress()
        filename = outfmt.format(mccCount)
        print(' writing to {}'.format(filename))
        nbrs.write(filename)
        mccCount += 1

        for t in cur:
            del vertAdj[t]


if __name__ == '__main__':
    main()
